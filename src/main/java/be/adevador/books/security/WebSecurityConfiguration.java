package be.adevador.books.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

@Configuration
@EnableWebSecurity

//TODO: @EnableWebMvcSecurity will be deprecated in spring boot 1.3.0, update


//  @EnableWebMvcSecurity actually adds the @EnableWebSecurity annotation in
//  WebMvcSecurityConfiguration.Therefore,@EnableWebMvcSecurity does everything that@EnableWebSecurity does,and a bit
//  more. If you look at WebMvcSecurityConfiguration,you will see that it adds an
//  AuthenticationPrincipalArgumentResolver so that you can access the authentication principal by adding an annotation
//  to a controller method argument. i.e.: @AuthenticationPrincipal parameter annotation in the GreetingController

@EnableWebMvcSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

//    The WebSecurityConfiguration class is annotated with @EnableWebMvcSecurity to enable Spring Security web security
//    support and provide the Spring MVC integration. It also extends WebSecurityConfigurerAdapter and overrides a
//    couple of its methods to set some specifics of the web security configuration.

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
//        Override this method to expose the AuthenticationManager from configure(AuthenticationManagerBuilder) to be
//        exposed as a Bean.
        return super.authenticationManagerBean();
    }

}
